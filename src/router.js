import Vue from 'vue';
import Router from 'vue-router';
import Contacts from './views/Messaging/Contacts.vue';
import AddContact from './views/Messaging/Add Contact.vue';
import SendMessage from './views/Messaging/Send Message'
import Banking from './views/Banking/Banking';
import Savings from './views/Banking/Savings';
import AddReminder from './views/Reminders/AddReminder';
import Login from './views/Login.vue';
import Reminders from './views/Reminders/Reminders';
import SendPayment from './views/Banking/SendPayment';
import MainFooter from './layout/MainFooter.vue';

Vue.use(Router);

export default new Router({
    mode: 'history',
    routes: [
        {
            path: '/',
            name: 'index',
            components: {default: Contacts, footer: MainFooter},
            props: {
                footer: {backgroundColor: 'black'}
            }
        },
        {
            path: '/add-contact',
            name: 'addContact',
            components: {default: AddContact, footer: MainFooter},
            props: {
                footer: {backgroundColor: 'black'}
            }
        },
        {
            path: '/send-message',
            name: 'sendMessage',
            components: {default: SendMessage, footer: MainFooter},
            props: {
                footer: {backgroundColor: 'black'}
            }
        },
        {
            path: '/banking',
            name: 'banking',
            components: {default: Banking, footer: MainFooter},
            props: {
                footer: {backgroundColor: 'black'}
            }
        },
        {
            path: '/savings',
            name: 'savings',
            components: {default: Savings, footer: MainFooter},
            props: {
                footer: {backgroundColor: 'black'}
            }
        },
        {
            path: '/send-payment',
            name: 'sendPayment',
            components: {default: SendPayment, footer: MainFooter},
            props: {
                footer: {backgroundColor: 'black'}
            }
        },
        {
            path: '/login',
            name: 'login',
            components: {default: Login},
        },
        {
            path: '/reminders',
            name: 'reminders',
            components: {default: Reminders, footer: MainFooter},
        },
        {
            path: '/add-reminder',
            name: 'addReminder',
            components: {default: AddReminder, footer: MainFooter},
        }
    ],
    scrollBehavior: to => {
        if (to.hash) {
            return {selector: to.hash};
        } else {
            return {x: 0, y: 0};
        }
    }
});
