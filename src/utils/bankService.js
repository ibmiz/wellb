import ApiConnectionHandler from './ApiConnectionHandler';
import {baseUrl} from '../config'

const apiEndpoint = baseUrl + '/bank-accounts';


export function sendMoneyToSavings(userId, amount) {
    console.log(userId, amount);
    return ApiConnectionHandler.post(apiEndpoint + '/to-savings', {
        userId, amount
    })
}

export function sendMoneyToAccount(userId, amount) {
    console.log(userId, amount);
    return ApiConnectionHandler.post(apiEndpoint + '/from-savings', {
        userId, amount
    })
}

export function sendMoneyToPayee(userId, amount, sortCode, accountNumber, payeeName) {
    return ApiConnectionHandler.post(apiEndpoint + '/to-payee', {
        userId, amount, sortCode, accountNumber, payeeName
    })
}

export function getAccountBalance(userId) {
    return ApiConnectionHandler.get(`${apiEndpoint}/current/${userId}`)
}

export function getSavingBalance(userId) {
    return ApiConnectionHandler.get(`${apiEndpoint}/savings/${userId}`)
}


