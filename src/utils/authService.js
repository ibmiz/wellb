import ApiConnectionHandler from './ApiConnectionHandler';
import {baseUrl} from '../config'

const apiEndpoint = baseUrl + '/auth';


export function loginUser(email, password) {
    return ApiConnectionHandler.post(apiEndpoint, {
        email, password
    })
}


