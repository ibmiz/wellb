import ApiConnectionHandler from './ApiConnectionHandler';
import {baseUrl} from '../config'

const apiEndpoint = baseUrl + '/contacts';


export function getContacts(userId) {
    console.log(userId);
    return ApiConnectionHandler.get(`${apiEndpoint}/${userId}`)
}

export function addUserContact(name, relationship, phone, userId) {
    console.log(name, relationship, phone, userId);
    return ApiConnectionHandler.post(apiEndpoint, {
        name, relationship, phone, userId
    })
}

export function sendMessageToContact(receiverId, text) {
    console.log(text);
    return ApiConnectionHandler.post(`${apiEndpoint}/send/${receiverId}`, {
        text
    })
}


