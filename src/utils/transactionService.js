import ApiConnectionHandler from './ApiConnectionHandler';
import {baseUrl} from '../config'

const apiEndpoint = baseUrl + '/transactions';


export function getTransactions(userId) {
    return ApiConnectionHandler.get(`${apiEndpoint}/${userId}`)
}


