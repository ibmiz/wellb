import ApiConnectionHandler from './ApiConnectionHandler';
import {baseUrl} from '../config'

const apiEndpoint = baseUrl + '/reminders';


export function getReminders(userId) {
    console.log(userId);
    return ApiConnectionHandler.get(`${apiEndpoint}/${userId}`)
}

export function addReminder(userId, text, hour, minute) {
    console.log(userId, text, hour, minute);
    return ApiConnectionHandler.post(`${apiEndpoint}/${userId}`, {
        text, hour, minute
    })
}

